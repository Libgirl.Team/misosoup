from setuptools import setup, find_packages

setup(name='misosoup',
      version='0.2.3',
      description='none',
      license='apache2',
      install_requires=['flask', 'gunicorn', 'dong>=0.2.1.6'],
      include_package_data=True,
      packages=find_packages(),
      zip_safe=False,
    )
