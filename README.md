# misosoup : a flask server script for serving dong's trained model
## Installation
1. Install user's dong ML project by pip
2. Under misosoup project folder root
```sh
$ pip install .
```

## Usage
1. Set ```MODEL_SAVE_DIR``` as the path to load the trained model.
2. Under ```misosoup/misosoup``` run
```sh
$ gunicorn 'misosoup:app(project_name="USER_PROJECT_NAME", service_module="USER_SERVICE_MODULE", service_class="USER_SERVICE_CLASS")'
```
It will listen to port 8000
## Test
### Ping
```sh
$ curl http://localhost:8000/api/v1 -v
...
< HTTP/1.1 204 NO CONTENT
```
### dong ML project
#### Set MODEL_SAVE_DIR
```sh
$ export MODEL_SAVE_DIR=[path-to-your-model-save-load-path]
```
#### Get sample dong project
For testing you can directly clone the [dong_mnist_example](https://bitbucket.org/libgirl_hai/dong_mnist_example/src/master/) 
```shell
$ git clone https://bitbucket.org/libgirl_hai/dong_mnist_example.git
```
See [dong_mnist_example](https://bitbucket.org/libgirl_hai/dong_mnist_example/src/master/) for how to train and output your model.
#### Launch misosoup
Then, launch the server under ```misosoup/misosoup``` by
```sh
$ gunicorn 'misosoup:app(project_name="dong_mnist_example")'
```
You should be able to use the simple test script to get the results with the same form.
(numbers can be different)
```shell
$ bash bin/simple-test.sh
[[2.879309568548649e-10, 2.6625946239478004e-11, 2.580107016925126e-09, 5.453760706930488e-12, 0.9999690055847168, 3.3259575649147166e-10, 3.2778924019538636e-10, 4.35676184906697e-07, 2.190379821964683e-10, 3.0488341508316807e-05], [1.3478038130010361e-10, 0.9997259974479675, 6.728584622806011e-08, 5.9139901864568856e-09, 9.023875122693426e-07, 5.708465922182882e-10, 1.2523435088951373e-07, 0.0002721738419495523, 6.667413003924594e-07, 9.076808638042166e-09]]
```
