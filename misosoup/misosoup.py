from flask import Flask
import flask
import importlib
import json
import os
import dong.framework


def create_app(Service):
    app = Flask(__name__)
    service = Service(save_dir=os.environ.get('MODEL_SAVE_DIR') + '/')

    @app.route('/api/v1/<call>', methods=['POST'])
    def respond_to_request(call):
        if call in dong.framework.get_request_handlers(Service,
                                                       name_only=True):
            method = getattr(service, call)
            json_body = flask.request.get_json(force=True)
            return app.response_class(response=method(json.dumps(json_body)),
                                      status=200,
                                      mimetype='application/json')
        else:
            return ('no such method', 404)

    @app.route('/api/v1', methods=['GET'])
    def ping():
        return app.response_class(status=204)

    return app


def app(project_name, service_module='default',
        service_class='DefaultService'):
    service_module = importlib.import_module(project_name + '.service.' +
                                             service_module)
    Service = getattr(service_module, service_class)
    return create_app(Service)
